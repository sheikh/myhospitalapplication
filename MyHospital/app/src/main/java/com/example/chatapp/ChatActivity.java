package com.example.chatapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

public class ChatActivity extends AppCompatActivity {

    String recieverId, recieverName, senderRoom, recieverRoom; // Declare variables for receiver ID, name, sender and receiver rooms
    DatabaseReference dbRefSender, dbRefReciver, userRef; // Declare database references for sender, receiver, and user
    ImageView btnsendMessageIcon; // Declare ImageView for send message button
    EditText messageSendText; // Declare EditText for message input
    RecyclerView recyclerView; // Declare RecyclerView for displaying messages
    MessageAdapter messageAdapter; // Declare MessageAdapter for RecyclerView


    @Override
    protected void onCreate(Bundle savedInstanceState) { // onCreate method
        super.onCreate(savedInstanceState); // Call superclass method
        EdgeToEdge.enable(this); // Enable edge-to-edge display
        setContentView(R.layout.activity_chat); // Set the content view to the activity layout

        Toolbar toolbar = findViewById(R.id.toolbar); // Find the toolbar by ID
        setSupportActionBar(toolbar); // Set the toolbar as the app bar

        userRef = FirebaseDatabase.getInstance().getReference("MyHospital/Users/Chats"); // Get Firebase reference for user chats
        recieverId = getIntent().getStringExtra("userID"); // Get receiver ID from intent
        recieverName = getIntent().getStringExtra("userName"); // Get receiver name from intent

        getSupportActionBar().setTitle(recieverName + "´s Chat"); // Set the title of the app bar



        // Get sender and receiver room IDs if receiver ID is not null
        if (recieverId != null) {
            senderRoom = FirebaseAuth.getInstance().getUid() + recieverId; // Create sender room ID
            recieverRoom = recieverId + FirebaseAuth.getInstance().getUid(); // Create receiver room ID
        }

        btnsendMessageIcon = findViewById(R.id.btnsendMessageIcon); // Find the send message button by ID
        messageAdapter = new MessageAdapter(this); // Initialize MessageAdapter
        recyclerView = findViewById(R.id.chatRecycler); // Find the RecyclerView by ID
        messageSendText = findViewById(R.id.messageEditSend); // Find the message input EditText by ID
        recyclerView.setAdapter(messageAdapter); // Set the adapter for RecyclerView
        recyclerView.setLayoutManager(new LinearLayoutManager(this)); // Set layout manager for RecyclerView


        //show all old Messages in chatroom for sender and reciever from Database
        // Get Firebase references for sender and receiver chats
        dbRefSender = FirebaseDatabase.getInstance().getReference("MyHospital/Users/Chats").child(senderRoom);
        dbRefReciver = FirebaseDatabase.getInstance().getReference("MyHospital/Users/Chats").child(recieverRoom);

        // Add event listener for new messages in sender's chat room
        dbRefSender.addValueEventListener(new ValueEventListener() {
            // Handle data change
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                // Create list to store messages
                List<MessageModel> messages = new ArrayList<>();
                // Iterate over message snapshots
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    // Get message model from snapshot
                    MessageModel messageModel = dataSnapshot.getValue(MessageModel.class);
                    // Add message to list
                    messages.add(messageModel);
                }
                // Sort messages by timestamp
                messages.sort(Comparator.comparingLong(MessageModel::getTimestamp));
                // Clear adapter
                messageAdapter.clear();
                // Add sorted messages to adapter
                for (MessageModel message : messages) {
                    messageAdapter.add(message);
                }
                // Notify adapter of data change
                messageAdapter.notifyDataSetChanged();

                // Scroll to the latest message
                recyclerView.scrollToPosition(messageAdapter.getItemCount() - 1);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Handle error (logging, showing a message, etc.)
            }
        });
        // Set click listener for send message button
        btnsendMessageIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Handle button click then get message text
                String message = messageSendText.getText().toString();
                //if there is a message written and is not empty
                if (!message.trim().isEmpty()) {
                    // Send the message
                    SendMessage(message);
                } else {
                    //Otherwise show toast if message is empty
                    Toast.makeText(ChatActivity.this, "No Message Written", Toast.LENGTH_SHORT).show();
                }
            }
        });


        //Original code to set window insets listener for edge-to-edge display
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    // Method to send a message by Generating unique ID for message and  Creating a message model
    private void SendMessage(String message) {
        String messageId = UUID.randomUUID().toString();
        MessageModel messageModel = new MessageModel(messageId, FirebaseAuth.getInstance().getUid(), message);
        messageAdapter.add(messageModel);// Add message to adapter and take the Names from the MessageModal

        // Add message to sender's chat room in database
        dbRefSender.child(messageId).setValue(messageModel)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    // Handling  success
                    @Override
                    public void onSuccess(Void unused) {
                        // Scroll to the latest message
                        recyclerView.scrollToPosition(messageAdapter.getItemCount() - 1);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    // Handle failure
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Show toast on failure
                        Toast.makeText(ChatActivity.this, "Failed to Send the MSG", Toast.LENGTH_SHORT).show();
                    }
                });
        // Add message to receiver's chat room in databas
        dbRefReciver.child(messageId).setValue(messageModel);
        // Clear message input
        messageSendText.setText("");
    }

    // Inflate menu options,Get menu inflater,Inflate chat menu and then
    // Return true to display the menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.chat_menu, menu);
        return true;
    }

    // Prepare menu options
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem shiftAnfrage = menu.findItem(R.id.ShiftAnfrage);
        MenuItem showAllShifts = menu.findItem(R.id.ShowAllShifts);
        MenuItem onDayCalender = menu.findItem(R.id.onDayCalender);

        // Logic to set visibility
        boolean isLoggedIn = checkUserLoggedIn(); // Replace with your actual logic
        shiftAnfrage.setVisible(isLoggedIn);
        showAllShifts.setVisible(!isLoggedIn);
        onDayCalender.setVisible(!isLoggedIn);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.ShiftAnfrage) {
            startActivity(new Intent(ChatActivity.this, MainActivity.class));
            finish();
            return true;
        }

        if(item.getItemId()== R.id.ShowAllShifts){
            startActivity(new Intent(ChatActivity.this, AllCalender.class));
            finish();
            return true;
        }
        if(item.getItemId()== R.id.listShift){
            startActivity(new Intent(ChatActivity.this, TextActivity.class));
            finish();
            return true;
        }
        if(item.getItemId()== R.id.onDayCalender){
            startActivity(new Intent(ChatActivity.this, MyCalender.class));
            finish();
            return true;
        }
        if (item.getItemId() == R.id.home) {
            startActivity(new Intent(ChatActivity.this, MainActivity.class));
            finish();
            return true;
        }
        return false;
    }

    // Check if user is logged in as ShiftsLeiter email
    // And return true if email matches.
    private boolean checkUserLoggedIn() {
        String loggedInUserEmail = getLoggedInUserEmail(); // Replace with actual logic to get the user's email
        return "loureen@gmail.com".equals(loggedInUserEmail);
    }

    // Get the logged in user's email
    private String getLoggedInUserEmail() {
        // Get current Firebase user and check If user is not null then return the user´s email
        // otherwise return null.
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            return user.getEmail();
        } else {
            return null;
        }
    }
}