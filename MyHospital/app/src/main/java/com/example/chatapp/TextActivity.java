package com.example.chatapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

public class TextActivity extends AppCompatActivity {

    TextAdapter textAdapter; // Declare TextAdapter
    DatabaseReference databaseReference; // Declare DatabaseReference
    private FirebaseAuth auth; // Declare FirebaseAuth
    private FirebaseDatabase firebaseDatabase; // Declare FirebaseDatabase
    private String loggedInUserEmail; // Declare string for logged in user's email

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        FirebaseApp.initializeApp(this);
        setContentView(R.layout.activity_text);

        // Find the toolbar by ID then set content then set title to empty
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle("");

        auth = FirebaseAuth.getInstance(); // Initialize FirebaseAuth
        firebaseDatabase = FirebaseDatabase.getInstance(); // Initialize FirebaseDatabase
        loggedInUserEmail = getCurrentUserEmail(); // Initialize logged in user's email

        // Fetch user's name and display welcome message
        // Get current Firebase user
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            String uid = currentUser.getUid();
            // Get reference to current user's data
            DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("MyHospital/Users").child(uid);

            // Add listener for single value event
            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    // Get user model from data snapshot
                    UserModel userModel = dataSnapshot.getValue(UserModel.class);
                    // Check if user model is not null
                    if (userModel != null) {
                        // Create welcome text
                        String welcomeText = userModel.getUserName().toUpperCase() + "`s Shifts";
                        // Find welcome TextView by ID
                        TextView tvWelcome = findViewById(R.id.tvWelcome);
                        tvWelcome.setText(welcomeText);// Set welcome text
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.w("Firebase", "Failed to read user"); // Log warning message
                }
            });
        }

        // Find back button by ID and start Main Activity if clicked
        Button btnBackshowShift = findViewById(R.id.btnBackshowShift);
        btnBackshowShift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TextActivity.this, MainActivity.class));
            }
        });



        textAdapter = new TextAdapter(this); // Initialize TextAdapter
        RecyclerView recyclerView = findViewById(R.id.recycler); // Find RecyclerView by ID
        recyclerView.setAdapter(textAdapter); // Set adapter for RecyclerView
        recyclerView.setLayoutManager(new LinearLayoutManager(this)); // Set layout manager for RecyclerView

        // Setup SearchView and Find SearchView by ID
        SearchView searchView = findViewById(R.id.searchView);
        // Set query text listener for SearchView
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) { // Handle query text submit
                textAdapter.filter(query); // Filter textAdapter with query
                return false; // Return false to indicate no further action is needed
            }

            @Override
            public boolean onQueryTextChange(String newText) { // Handle query text change
                textAdapter.filter(newText); // Filter textAdapter with newText
                return false; // Return false to indicate no further action is needed
            }
        });

        // Call method to fetch shifts
        fetchShifts();

        //Original code to set window insets listener for edge-to-edge display
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    private void fetchShifts() { // Method to fetch shifts
        if (checkIfAdmin()) { // Check if the current user is an admin
            // Fetch all shifts if the user is admin
            databaseReference = firebaseDatabase.getReference().child("MyHospital/Users/Shifts"); // Get reference to all shifts
            databaseReference.addValueEventListener(new ValueEventListener() { // Add value event listener
                @SuppressLint("NotifyDataSetChanged") // Suppress lint warning
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) { // Handle data change
                    textAdapter.clear(); // Clear text adapter
                    for (DataSnapshot userSnapshot : snapshot.getChildren()) { // Iterate over user snapshots
                        for (DataSnapshot shiftSnapshot : userSnapshot.getChildren()) { // Iterate over shift snapshots
                            TextModel textModel = shiftSnapshot.getValue(TextModel.class); // Get TextModel from shift snapshot
                            if (textModel != null && textModel.getTime() != null) { // Check if TextModel is valid
                                textAdapter.add(textModel); // Add TextModel to text adapter
                            }
                        }
                    }
                    textAdapter.notifyDataSetChanged(); // Notify adapter of data change
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) { // Handle database error
                    Toast.makeText(TextActivity.this, "Database error: " + error.getMessage(), Toast.LENGTH_SHORT).show(); // Show database error toast
                }
            });
        } else { // If the user is not an admin
            // Fetch only the logged-in user's shifts if the user is not admin
            FirebaseUser currentUser = auth.getCurrentUser(); // Get current Firebase user
            if (currentUser != null) { // Check if current user is not null
                String userId = currentUser.getUid(); // Get current user's UID
                databaseReference = firebaseDatabase.getReference().child("MyHospital/Users/Shifts").child(userId); // Get reference to current user's shifts
                databaseReference.addValueEventListener(new ValueEventListener() { // Add value event listener
                    @SuppressLint("NotifyDataSetChanged") // Suppress lint warning
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) { // Handle data change
                        textAdapter.clear(); // Clear text adapter
                        for (DataSnapshot dataSnapshot : snapshot.getChildren()) { // Iterate over shift snapshots
                            TextModel textModel = dataSnapshot.getValue(TextModel.class); // Get TextModel from shift snapshot
                            if (textModel != null && textModel.getTime() != null) { // Check if TextModel is valid
                                textAdapter.add(textModel); // Add TextModel to text adapter
                            }
                        }
                        textAdapter.notifyDataSetChanged(); // Notify adapter of data change
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) { // Handle database error
                        Toast.makeText(TextActivity.this, "Database error: " + error.getMessage(), Toast.LENGTH_SHORT).show(); // Show database error toast
                    }
                });
            } else {
                Log.e("TextActivity", "User is not authenticated"); // Log authentication error
                // Optionally, redirect to login screen or show a message to the user
            }
        }
    }


    private boolean checkIfAdmin() { // Method to check if the current user is an admin
        String currentUserEmail = getCurrentUserEmail(); // Get current user's email
        return currentUserEmail != null && currentUserEmail.equalsIgnoreCase("loureen@gmail.com"); // Check if email matches admin email
    }

    private String getCurrentUserEmail() { // Method to get the current user's email
        FirebaseUser user = auth.getCurrentUser(); // Get current Firebase user
        return user != null ? user.getEmail() : null; // Return user's email if user is not null, otherwise return null
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) { // Method to create options menu
        getMenuInflater().inflate(R.menu.menu_shifts, menu); // Inflate menu with menu_shifts
        return true; // Return true to display the menu
    }


    // Method to prepare the options menu by Find icons menu item by ID

    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem AddMessage = menu.findItem(R.id.AddMessage);
        MenuItem onDayCalender = menu.findItem(R.id.onDayCalender);
        MenuItem allShiftsCalender = menu.findItem(R.id.allShiftsCalender);
        MenuItem logout = menu.findItem(R.id.logout);
        MenuItem shiftAnfrage = menu.findItem(R.id.ShiftAnfrage);


        // Logic to set visibility
        boolean isLoggedIn = checkUserLoggedIn(); // Check if the user is logged in
        shiftAnfrage.setVisible(isLoggedIn); // Set visibility of shiftAnfrage based on login status
        onDayCalender.setVisible(!isLoggedIn); // Set visibility of onDayCalender based on login status
        // Call superclass method
        return super.onPrepareOptionsMenu(menu);
    }

    // Method to handle options item selection and start related activitys
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.logout) {
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(this, SignInActivity.class));
            finish();
            return true;
        }
        if (item.getItemId() == R.id.AddMessage) {
            startActivity(new Intent(TextActivity.this, MainActivity.class));
            finish();
            return true;
        }
        if (item.getItemId() == R.id.onDayCalender) {
            startActivity(new Intent(TextActivity.this, MyCalender.class));
            finish();
            return true;
        }

        if (item.getItemId() == R.id.allShiftsCalender) {
            startActivity(new Intent(TextActivity.this, AllCalender.class));
            finish();
            return true;
        }
        if (item.getItemId() == R.id.ShiftAnfrage) {
            startActivity(new Intent(TextActivity.this, RequistChange.class));
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Method to check if the user is logged in is the same as Shift Lieder Email
    private boolean checkUserLoggedIn() {
        String loggedInUserEmail = getLoggedInUserEmail(); // Replace with actual logic to get the user's email
        return "loureen@gmail.com".equals(loggedInUserEmail);
    }

    // Method to get the logged-in user's email
    private String getLoggedInUserEmail() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            return user.getEmail();
        } else {
            return null;
        }
    }
}
