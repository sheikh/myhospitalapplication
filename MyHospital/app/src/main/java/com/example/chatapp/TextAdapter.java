package com.example.chatapp;// Package declaration
// Importing All necessery library
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

// Define TextAdapter class extending RecyclerView.Adapter
public class TextAdapter extends RecyclerView.Adapter<TextAdapter.TextViewHolder> {
    private static final String CHANNEL_ID = "shift_notifications"; // Declare constant for notification channel ID
    private Context context; // Declare context
    private List<TextModel> textModelList; // Declare list for text models
    private List<TextModel> textModelListFull; // Declare list for full text models
    private FirebaseAuth auth; // Declare FirebaseAuth
    private FirebaseDatabase firebaseDatabase; // Declare FirebaseDatabase
    private DatabaseReference databaseReference; // Declare DatabaseReference
    private String loggedInUserEmail; // Declare string for logged-in user's email


    // Constructor for TextAdapter
    public TextAdapter(Context context) {
        this.context = context; // Initialize context
        this.textModelList = new ArrayList<>(); // Initialize text model list
        this.textModelListFull = new ArrayList<>(); // Initialize full text model list
        this.auth = FirebaseAuth.getInstance(); // Initialize FirebaseAuth
        this.firebaseDatabase = FirebaseDatabase.getInstance(); // Initialize FirebaseDatabase
        this.databaseReference = firebaseDatabase.getReference("MyHospital/Users/Shifts"); // Initialize DatabaseReference
        this.loggedInUserEmail = getCurrentUserEmail(); // Initialize logged-in user's email
        createNotificationChannel(); // Create the notification channel
    }

    // Method to add a text model
    public void add(TextModel textModel) {
        textModelList.add(textModel); // Add text model to list
        textModelListFull.add(textModel); // Add text model to full list
        sortTextModelList(); // Sort the list after adding a new shift
        notifyDataSetChanged(); // Notify adapter of data change
        showNotification(textModel); // Show notification for new shift
    }


    // Method to clear text models lists
    public void clear() {
        textModelList.clear(); // Clear text model list
        textModelListFull.clear(); // Clear full text model list
        notifyDataSetChanged(); // Notify adapter of data change
    }

    @NonNull
    @Override
    public TextViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.main_shift, parent, false);
        return new TextViewHolder(view);
    }


    // Bind data to ViewHolder
    @Override
    public void onBindViewHolder(@NonNull TextViewHolder holder, int position) {
        // Get TextModel at position
        TextModel textModel = textModelList.get(position);

        holder.timeAdded.setText(textModel.getTime().toLowerCase().trim()); // Set time added
        holder.ShiftsID.setText(textModel.getShiftsID().toLowerCase().trim()); // Set ShiftsID
        holder.UserName.setText(textModel.getUserName().toUpperCase().trim()); // Set user name
        holder.dateFrom.setText(textModel.getDateFrom().toLowerCase().trim()); // Set date from
        holder.department.setText(textModel.getDepartment().toLowerCase().trim()); // Set department
        holder.shiftSelection.setText(textModel.getShiftSelection().toLowerCase().trim()); // Set shift selection
        holder.details.setText(textModel.getDetails().toLowerCase().trim()); // Set details
        holder.colleg.setText(textModel.getColleg().toLowerCase().trim()); // Set colleagues
        holder.SectionSelect.setText(textModel.getSectionSelect().toLowerCase().trim()); // Set section select

        holder.itemView.setOnClickListener(v -> { // Set item click listener
            Intent intent = new Intent(context, MainActivity.class); // Create intent for MainActivity
            intent.putExtra("userID", textModel.getUserID()); // Pass user ID to intent
            intent.putExtra("ShiftsID", textModel.getShiftsID()); // Pass ShiftsID to intent
            context.startActivity(intent); // Start MainActivity
        });


        if (auth.getCurrentUser() != null && "loureen@gmail.com".equals(auth.getCurrentUser().getEmail())) { // Check if current user is admin
            holder.btnDeleteShift.setVisibility(View.VISIBLE); // Show delete shift button
            holder.btnSendRequist.setVisibility(View.GONE); // Hide send request button
        } else {
            holder.btnDeleteShift.setVisibility(View.GONE); // Hide delete shift button
            holder.btnSendRequist.setVisibility(View.VISIBLE); // Show send request button
        }

        holder.btnDeleteShift.setOnClickListener(v -> showDeleteConfirmationDialog(textModel, holder)); // Set delete shift button click listener
        holder.btnSendRequist.setOnClickListener(v -> { // Set send request button click listener
            final DialogPlus dialogPlus = DialogPlus.newDialog(holder.UserName.getContext()) // Create new DialogPlus dialog
                    .setContentHolder(new ViewHolder(R.layout.popup_shift_requist)) // Set content holder to popup_shift_requist layout
                    .setExpanded(true, 2800) // Set dialog to be expanded with height of 2800
                    .create();

            dialogPlus.show(); // Show dialog
            View view = dialogPlus.getHolderView(); // Get dialog view

            TextView shiftsID = view.findViewById(R.id.ShiftsID); // Find ShiftsID TextView by ID
            TextView userName = view.findViewById(R.id.username); // Find username TextView by ID
            TextView time = view.findViewById(R.id.myTime); // Find time TextView by ID
            TextView dateFrom = view.findViewById(R.id.DateFrom); // Find dateFrom TextView by ID
            TextView shiftSelection = view.findViewById(R.id.pShiftSelection); // Find shift selection TextView by ID
            TextView department = view.findViewById(R.id.pDepartment); // Find department TextView by ID
            EditText shiftDetails = view.findViewById(R.id.pShiftDetails); // Find shift details EditText by ID
            Spinner changeRequistSpinner = view.findViewById(R.id.ChangeRequist); // Find change request Spinner by ID


            ArrayAdapter<CharSequence> adapterChangeRequist = ArrayAdapter.createFromResource(holder.UserName.getContext(),
                    R.array.Change_Requist, android.R.layout.simple_spinner_item); // Create adapter for change request spinner
            adapterChangeRequist.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // Set dropdown view resource
            changeRequistSpinner.setAdapter(adapterChangeRequist); // Set adapter for change request spinner

            // Find add change dialog button by ID
            Button btnAddChangeDialog = view.findViewById(R.id.btnAddChangeDialog);


            shiftsID.setText(textModel.getShiftsID()); // Set ShiftsID TextView
            department.setText(textModel.getDepartment()); // Set department TextView
            shiftSelection.setText(textModel.getShiftSelection()); // Set shift selection TextView
            userName.setText(textModel.getUserName()); // Set username TextView
            dateFrom.setText(textModel.getDateFrom()); // Set dateFrom TextView

            // Check if time is not null set time TextView
            if (textModel.getTime() != null) {
                time.setText(textModel.getTime());
            } else {
                String currentTime = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.getDefault()).format(new Date());
                time.setText(currentTime);
            }

            // Set click listener for add change dialog button
            btnAddChangeDialog.setOnClickListener(v1 -> {
                btnAddChangeDialog.setEnabled(false); // Disable the button to prevent multiple clicks
                String currentTime = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.getDefault()).format(new Date()); // Get the current time
                String changeRequist = changeRequistSpinner.getSelectedItem().toString(); // Get the selected change request
                Map<String, Object> map = new HashMap<>(); // Create a map to store shift details
                map.put("ShiftsID", shiftsID.getText().toString().toLowerCase()); // Put ShiftsID in the map
                map.put("Name", userName.getText().toString().toLowerCase()); // Put name in the map
                map.put("DateFrom", dateFrom.getText().toString().toLowerCase()); // Put date from in the map
                map.put("Department", department.getText().toString().toLowerCase()); // Put department in the map
                map.put("ShiftSelection", shiftSelection.getText().toString().toLowerCase()); // Put shift selection in the map
                map.put("ChangeRequist", changeRequist.toLowerCase()); // Put change request in the map
                map.put("Details", shiftDetails.getText().toString().toLowerCase()); // Put details in the map
                map.put("Time", currentTime); // Put current time in the map

                databaseReference.child("ChangeRequist").child(auth.getCurrentUser().getUid()) // Reference to ChangeRequist node in database
                        .push().setValue(map) // Push the map to the database
                        .addOnSuccessListener(unused -> { // Add success listener
                            Toast.makeText(context, "Requist Added Successfully", Toast.LENGTH_LONG).show(); // Show success toast
                            dialogPlus.dismiss(); // Dismiss the dialog
                        }).addOnFailureListener(e -> { // Add failure listener
                            Toast.makeText(holder.UserName.getContext(), "Error while Adding Shift", Toast.LENGTH_SHORT).show(); // Show error toast
                            dialogPlus.dismiss(); // Dismiss the dialog
                        });
            });
        });


    }

    private void showDeleteConfirmationDialog(TextModel textModel, TextViewHolder holder) { // Method to show delete confirmation dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(holder.UserName.getContext()); // Create AlertDialog builder
        builder.setTitle("Are you sure to DELETE " + textModel.getUserName()); // Set dialog title
        builder.setMessage("Deleted data can't be undone"); // Set dialog message
        builder.setPositiveButton("Delete", (dialog, which) -> { // Set positive button and click listener
            deleteRequistFromFirebase(textModel.getUserID(), textModel.getShiftsID()); // Call method to delete request from Firebase
            removeUser(textModel); // Call method to remove user from adapter
            Toast.makeText(holder.UserName.getContext(), "Request is Deleted", Toast.LENGTH_SHORT).show(); // Show deletion toast
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> // Set negative button and click listener
                Toast.makeText(holder.UserName.getContext(), "Cancelled", Toast.LENGTH_SHORT).show() // Show cancellation toast
        );
        builder.show(); // Show the dialog
    }

    private void removeUser(TextModel textModel) { // Method to remove user from adapter
        textModelList.remove(textModel); // Remove text model from list
        textModelListFull.remove(textModel); // Remove text model from full list
        notifyDataSetChanged(); // Notify adapter of data change
    }

    // Method to delete request from Firebase by Checking  if user ID and shift ID are valid
    private void deleteRequistFromFirebase(String userId, String shiftID) {
        if (userId != null && !userId.isEmpty() && shiftID != null && !shiftID.isEmpty()) {
            // Remove value from database
            databaseReference.child(userId).child(shiftID).removeValue().addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Toast.makeText(context, "Request successfully deleted", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Request deletion failed", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(context, "Invalid User ID or Shift ID", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int getItemCount() {
        return textModelList.size();
    }


    // Method to filter the textModelList based on search text
    @SuppressLint("NotifyDataSetChanged")
    public void filter(String text) {
        textModelList.clear();// Clear the current list
        if (text.trim().isEmpty()) {
            textModelList.addAll(textModelListFull);
        } else {
            String[] searchTerms = text.toLowerCase().trim().split("\\s+"); // Split the search text into terms
            for (TextModel item : textModelListFull) { // Iterate over the full list
                for (String term : searchTerms) { // Iterate over search terms
                    if (item.getDepartment().toLowerCase().contains(term) || // Check if term is in department
                            item.getDateFrom().toLowerCase().contains(term) || // Check if term is in dateFrom
                            item.getUserName().toLowerCase().contains(term) || // Check if term is in userName
                            item.getShiftSelection().toLowerCase().contains(term) || // Check if term is in shiftSelection
                            item.getColleg().toLowerCase().contains(term) || // Check if term is in colleg
                            item.getDetails().toLowerCase().contains(term)) { // Check if term is in details
                        textModelList.add(item); // Add item to filtered list
                        break; // Break inner loop if term matches
                    }
                }
            }
        }
        // Notify adapter of data change
        notifyDataSetChanged();
    }

    private void sortTextModelList() { // Method to sort the textModelList
        Collections.sort(textModelList, (o1, o2) -> { // Sort the list using a comparator
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()); // Date format
            try {
                Date date1 = sdf.parse(o1.getDateFrom()); // Parse date from o1
                Date date2 = sdf.parse(o2.getDateFrom()); // Parse date from o2
                if (date1 != null && date2 != null) { // Check if dates are not null
                    return date2.compareTo(date1); // Compare dates in descending order
                }
            } catch (ParseException e) {
                e.printStackTrace(); // Print stack trace for parse exception
            }
            return 0; // Return 0 if comparison fails
        });
    }

    private String getCurrentUserEmail() { // Method to get the current user's email
        if (auth.getCurrentUser() != null) { // Check if current user is not null
            return auth.getCurrentUser().getEmail(); // Return user's email
        }
        return null; // Return null if user is null
    }


    // Define TextViewHolder class extending RecyclerView.ViewHolder
    public class TextViewHolder extends RecyclerView.ViewHolder {
        private TextView dateFrom, timeAdded, department, shiftSelection, details, colleg, UserName, ShiftsID, SectionSelect; // Declare TextViews
        ImageButton btnDeleteShift, btnSendRequist; // Declare ImageButtons

        // Constructor for TextViewHolder
        public TextViewHolder(@NonNull View itemView) {
            super(itemView);
            dateFrom = itemView.findViewById(R.id.userDateFrom);
            ShiftsID = itemView.findViewById(R.id.ShiftsID);
            UserName = itemView.findViewById(R.id.username);
            timeAdded = itemView.findViewById(R.id.userTime);
            department = itemView.findViewById(R.id.userDepartment);
            shiftSelection = itemView.findViewById(R.id.userSchichtTime);
            details = itemView.findViewById(R.id.userShiftDetails);
            colleg = itemView.findViewById(R.id.userShiftColleg);
            SectionSelect = itemView.findViewById(R.id.userRoomSelection);
            btnDeleteShift = itemView.findViewById(R.id.btnDeleteShift);
            btnSendRequist = itemView.findViewById(R.id.btnSendRequist);

        }
    }
    // Method to create notification channel
    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Shift Notifications";
            String description = "Notifications for new shifts";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    // Displays a notification to the user if the provided textModel belongs to the currently authenticated user.
    private void showNotification(TextModel textModel) {
        // Check if the notification is for the currently authenticated user
        if (textModel.getUserID().equals(FirebaseAuth.getInstance().getUid())) {
            // Create an intent to launch the MyCalendar activity
            Intent intent = new Intent(context, MyCalender.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            // Create a TaskStackBuilder to manage the back stack for the notification's intent
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addNextIntentWithParentStack(intent);

            // Create a PendingIntent for the notification's action
            PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

            // Build the notification using NotificationCompat
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                    .setSmallIcon(R.drawable.calenderadd) // Set the notification icon
                    .setContentTitle("You have New Shift") // Set the notification title
                    .setContentText("On : " + textModel.getDateFrom() + "\n In: " + textModel.getDepartment() + "\n At: " + textModel.getShiftSelection()) // Set the notification text
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT) // Set the notification priority
                    .setContentIntent(pendingIntent) // Set the PendingIntent to be triggered when the notification is clicked
                    .setAutoCancel(true); // Automatically remove the notification when it is clicked

            // Get the NotificationManager service to display the notification
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            // Define a unique ID for this notification type
            int notificationId = 2; // Use a constant ID for TextModel notifications
            // Show the notification
            notificationManager.notify(notificationId, builder.build());
        }
    }



}
