package com.example.chatapp;

// Define MessageModel class to provides a structured way to store and manage message data.
// This makes it easy to pass message objects around the application,
// such as saving them to a database or displaying them in a UI.
public class MessageModel {

    private String messageId, senderID, message,RecieverID;
    private long timestamp;

    // Default constructor required for calls to DataSnapshot.getValue(MessageModel.class)
    public MessageModel() {
    }

    public MessageModel(String messageId, String senderID, String message) { // Constructor for MessageModel
        this.messageId = messageId; // Initialize message ID
        this.senderID = senderID; // Initialize sender ID
        this.message = message; // Initialize message
        this.timestamp = System.currentTimeMillis(); // Initialize timestamp with current time
    }

    // All Setter and Gitter
    public String getRecieverID() {
        return RecieverID;
    }

    public void setRecieverID(String recieverID) {
        RecieverID = recieverID;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getSenderID() {
        return senderID;
    }

    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
