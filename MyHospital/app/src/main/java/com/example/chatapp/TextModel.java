package com.example.chatapp;


// Define TextModel class to provides a structured way to store and manage Shifts Addition data.
// This makes it easy to pass the Shifts objects around the application,
// such as saving them to a database or displaying them in a UI.
public class TextModel {

    private String DateFrom,Time,Department,ShiftSelection,Details,Colleg,ShiftsID;

    private String UserName,UserID,SectionSelect;


    // Default constructor required for calls to DataSnapshot
    public TextModel() {
    }

    public TextModel(String SectionSelect,String userID,String dateFrom,String shiftsID, String time, String department, String shiftSelection, String details, String colleg,String UserName) {
        DateFrom = dateFrom;
        Time = time;
        Department = department;
        ShiftSelection = shiftSelection;
        Details = details;
        Colleg = colleg;
        ShiftsID= shiftsID;
        UserName= UserName;
        UserID=userID;
        SectionSelect=SectionSelect;

    }

    public String getSectionSelect() {
        return SectionSelect;
    }

    public void setSectionSelect(String sectionSelect) {
        SectionSelect = sectionSelect;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getShiftsID() {
        return ShiftsID;
    }

    public void setShiftsID(String shiftsID) {
        ShiftsID = shiftsID;
    }

    public String getDateFrom() {
        return DateFrom;
    }

    public void setDateFrom(String dateFrom) {
        DateFrom = dateFrom;
    }



    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getDepartment() {
        return Department;
    }

    public void setDepartment(String department) {
        Department = department;
    }

    public String getShiftSelection() {
        return ShiftSelection;
    }

    public void setShiftSelection(String shiftSelection) {
        ShiftSelection = shiftSelection;
    }

    public String getDetails() {
        return Details;
    }

    public void setDetails(String details) {
        Details = details;
    }

    public String getColleg() {
        return Colleg;
    }

    public void setColleg(String colleg) {
        Colleg = colleg;
    }

//    public String getUserName() {
//        return Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getDisplayName();
//    }
//
//    public void setUserName(String userName) {
//        this.userName = userName;
//    }
//


    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        this.UserName = userName;
    }

}
