package com.example.chatapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class RequistAdapter extends RecyclerView.Adapter<RequistAdapter.RequistViewHolder> {
    private Context context; // Declare context
    private List<RequistModel> requistModelList; // Declare list for request models
    private List<RequistModel> requistModelListFull; // Declare list for full request models
    private FirebaseAuth auth; // Declare FirebaseAuth
    private DatabaseReference databaseReference; // Declare DatabaseReference
    private SharedPreferences sharedPreferences; // Declare SharedPreferences

    // Constructor for RequistAdapter
    public RequistAdapter(Context context) {
        this.context = context; // Initialize context
        this.requistModelList = new ArrayList<>(); // Initialize request model list
        this.requistModelListFull = new ArrayList<>(requistModelList); // Initialize full request model list
        this.sharedPreferences = context.getSharedPreferences("RequistPrefs", Context.MODE_PRIVATE); // Initialize SharedPreferences
        this.auth = FirebaseAuth.getInstance(); // Initialize FirebaseAuth
        this.databaseReference = FirebaseDatabase.getInstance().getReference("MyHospital/Users/Shifts/ChangeRequist"); // Initialize DatabaseReference

        fetchDataFromFirebase(); // Fetch data from Firebase
    }

    // Fetch data from Firebase
    private void fetchDataFromFirebase() {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                requistModelList.clear(); // Clear request model list
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) { // Iterate over user snapshots
                    for (DataSnapshot requistSnapshot : userSnapshot.getChildren()) { // Iterate over request snapshots
                        RequistModel requistModel = requistSnapshot.getValue(RequistModel.class); // Get request model
                        if (requistModel != null) { // If request model is not null
                            requistModel.setRequistID(requistSnapshot.getKey()); // Set request ID
                            requistModel.setUserID(userSnapshot.getKey()); // Set user ID
                            requistModelList.add(requistModel); // Add request model to list
                        }
                    }
                }
                requistModelListFull.clear(); // Clear full request model list
                requistModelListFull.addAll(requistModelList); // Add all request models to full list
                sortRequistModelList(); // Sort the list after fetching data
                notifyDataSetChanged(); // Notify adapter of data change
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Handle database error
                Log.e("RequistAdapter", "Database error: " + databaseError.getMessage());
            }
        });
    }

    // Method to add a request model
    @SuppressLint("NotifyDataSetChanged") // Suppress lint warning
    public void add(RequistModel requistModel) {
        // Add request model to list
        requistModelList.add(requistModel);
        // Add request model to full list
        requistModelListFull.add(requistModel);
        // Sort the list after adding a new request
        sortRequistModelList();
        // Notify adapter of data change
        notifyDataSetChanged();
    }

    @SuppressLint("NotifyDataSetChanged") // Suppress lint warning
    public void clear() { // Method to clear request models
        requistModelList.clear(); // Clear request model list
        requistModelListFull.clear(); // Clear full request model list
        notifyDataSetChanged(); // Notify adapter of data change
    }

    // Inflate request row layout
    @NonNull
    @Override
    public RequistViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.requist_row, parent, false);
        return new RequistViewHolder(view); // Return view holder
    }

    @Override
    public void onBindViewHolder(@NonNull RequistViewHolder holder, int position) {
        // Get request model for position
        RequistModel requistModel = requistModelList.get(position);

        holder.Name.setText(requistModel.getName().toLowerCase().trim()); // Set name
        holder.ShiftsID.setText(requistModel.getShiftsID()); // Set shifts ID
        holder.DateFrom.setText(requistModel.getDateFrom().toLowerCase().trim()); // Set date from
        holder.Time.setText(requistModel.getTime().toLowerCase().trim()); // Set time
        holder.Department.setText(requistModel.getDepartment().toLowerCase().trim()); // Set department
        holder.ShiftSelection.setText(requistModel.getShiftSelection().toLowerCase().trim()); // Set shift selection
        holder.Details.setText(requistModel.getDetails().toLowerCase().trim()); // Set details
        holder.ChangeRequist.setText(requistModel.getChangeRequist().toLowerCase().trim()); // Set change request

        // Generate a unique key for each checkbox using ShiftsID
        String checkboxKey = "checkbox_" + requistModel.getShiftsID();


        boolean isChecked = sharedPreferences.getBoolean(checkboxKey, false); // Get checkbox state from SharedPreferences
        holder.requistCheckBox.setChecked(isChecked); // Set checkbox state
        if (isChecked) { // If checkbox is checked
            holder.textLayout.setBackgroundColor(Color.parseColor("#E7CCCC")); // Set background color
        } else { // If checkbox is not checked
            holder.textLayout.setBackgroundColor(Color.parseColor("#5E6A69")); // Set background color
        }


        holder.requistCheckBox.setOnCheckedChangeListener((buttonView, isChecked1) -> { // Set checkbox change listener
            SharedPreferences.Editor editor = sharedPreferences.edit(); // Get SharedPreferences editor
            editor.putBoolean(checkboxKey, isChecked1); // Save checkbox state
            editor.apply(); // Apply changes
            if (isChecked1) { // If checkbox is checked
                holder.textLayout.setBackgroundColor(Color.parseColor("#E7CCCC")); // Set background color
            } else { // If checkbox is not checked
                holder.textLayout.setBackgroundColor(Color.parseColor("#5E6A69")); // Set background color
            }
        });
    }


    // Get item count and Return size of request model list
    @Override
    public int getItemCount() {
        return requistModelList.size();
    }



    // Sort request model list by date and define date format
    private void sortRequistModelList() {
        Collections.sort(requistModelList, (o1, o2) -> {
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
            try {
                Date date1 = sdf.parse(o1.getDateFrom());
                Date date2 = sdf.parse(o2.getDateFrom());
                if (date1 != null && date2 != null) {
                    return date2.compareTo(date1); // Descending order
                }
                // Handle parse exception and Print stack trace
            } catch (ParseException e) {
                e.printStackTrace();
            }
            // Return 0 if comparison cannot be made
            return 0;
        });
    }

    // Define RequistViewHolder class
    public class RequistViewHolder extends RecyclerView.ViewHolder {
        private TextView Name, DateFrom, Time, Department, ShiftSelection, Details, ChangeRequist, ShiftsID; // Declare TextViews
        public CheckBox requistCheckBox; // Declare CheckBox
        public RelativeLayout textLayout; // Declare RelativeLayout

        // Constructor for RequistViewHolder
        public RequistViewHolder(@NonNull View itemView) {
            super(itemView); // Call superclass constructor
            ShiftsID = itemView.findViewById(R.id.userid); // Find shifts ID TextView by ID
            Name = itemView.findViewById(R.id.username); // Find name TextView by ID
            DateFrom = itemView.findViewById(R.id.userDateFrom); // Find date from TextView by ID
            Time = itemView.findViewById(R.id.userTime); // Find time TextView by ID
            Department = itemView.findViewById(R.id.userDepartment); // Find department TextView by ID
            ShiftSelection = itemView.findViewById(R.id.userSchichtTime); // Find shift selection TextView by ID
            Details = itemView.findViewById(R.id.userShiftDetails); // Find details TextView by ID
            ChangeRequist = itemView.findViewById(R.id.requistChange); // Find change request TextView by ID
            requistCheckBox = itemView.findViewById(R.id.requistCheckBox); // Find request CheckBox by ID
            textLayout = itemView.findViewById(R.id.textLayout); // Find text layout by ID
        }
    }
}
