package com.example.chatapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

// Define RequistChange class extending AppCompatActivity
public class RequistChange extends AppCompatActivity {
    DatabaseReference databaseReference; // Declare DatabaseReference for Firebase
    RequistAdapter requistAdapter; // Declare RequistAdapter for RecyclerView
    private FirebaseAuth auth; // Declare FirebaseAuth
    private FirebaseDatabase firebaseDatabase; // Declare FirebaseDatabase


    // onCreate method,Enable edge-to-edge display, Initialize Firebase app and  Set the content view to the activity layout
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        FirebaseApp.initializeApp(this);
        setContentView(R.layout.activity_requist_change);


        Toolbar toolbar = findViewById(R.id.toolbar); // Find the toolbar by ID
        setSupportActionBar(toolbar); // Set the toolbar as the app bar
        Objects.requireNonNull(getSupportActionBar()).setTitle(""); // Set the toolbar title to empty


        auth = FirebaseAuth.getInstance(); // Initialize FirebaseAuth
        firebaseDatabase = FirebaseDatabase.getInstance(); // Initialize FirebaseDatabase


        // Initialize RequestAdapter
        requistAdapter= new RequistAdapter(this);
        // Initialize FirebaseDatabase
        RecyclerView recyclerView = findViewById(R.id.recycler);
        // Initialize RequestAdapter
        recyclerView.setAdapter(requistAdapter);
        // Find the RecyclerView by ID
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Fetch Content (requests) from Firebase
        fetchRequist();



        //Original code to set window insets listener for edge-to-edge display
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    // Fetch requests from Firebase
    private void fetchRequist() {
        // Get reference to change requests from database
        databaseReference = firebaseDatabase.getReference().child("MyHospital/Users/Shifts/ChangeRequist");
        // Add value event listener
        databaseReference.addValueEventListener(new ValueEventListener() {
            // Handle data change
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                requistAdapter.clear();// Clear adapter
                for (DataSnapshot userSnapshot : snapshot.getChildren()) { // Iterate over user snapshots
                    for (DataSnapshot shiftSnapshot : userSnapshot.getChildren()) { // Iterate over shift snapshots
                        RequistModel requistModel = shiftSnapshot.getValue(RequistModel.class); // Get request model
                        if (requistModel != null && requistModel.getTime() != null) { // If request model is valid
                            requistAdapter.add(requistModel); // Add request model to adapter
                        }
                    }
                }
                // Notify adapter of data change
                requistAdapter.notifyDataSetChanged();
            }

            // Handle database error
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(RequistChange.this, "Database error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    // Initialize menu and Get menu inflater
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater= getMenuInflater();
        inflater.inflate(R.menu.menu_requist,menu);
        return true; // Return true to display the menu
    }




    // handling menu icons and there functionality
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()== R.id.logout){
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(RequistChange.this,SignInActivity.class));
            finish();
            return true;
        }
        if(item.getItemId()== R.id.home){
            FirebaseAuth.getInstance();
            startActivity(new Intent(RequistChange.this, MainActivity.class));
            finish();
            return true;
        }

        if(item.getItemId()== R.id.ShowAllShifts){
            FirebaseAuth.getInstance();
            startActivity(new Intent(RequistChange.this, AllCalender.class));
            finish();
            return true;
        }

        if(item.getItemId()== R.id.listShift){

            FirebaseAuth.getInstance();
            startActivity(new Intent(RequistChange.this, TextActivity.class));
            finish();
            return true;
        }


        return false;

    }



}