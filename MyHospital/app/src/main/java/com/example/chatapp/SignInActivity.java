package com.example.chatapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApiNotAvailableException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

// Define SignInActivity class extending AppCompatActivity
public class SignInActivity extends AppCompatActivity {
    EditText userEmail, userPassword; // Declare EditTexts for user email and password
    TextView btnLogin, btnSignup; // Declare TextViews for login and signup buttons
    String email, passsword; // Declare strings for email and password
    DatabaseReference databaseReference; // Declare DatabaseReference for Firebase

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_signin);


        //get Referance to see EveryThing in DataBase
        databaseReference = FirebaseDatabase.getInstance().getReference("MyHospital/Users");


        userEmail = findViewById(R.id.emailLogin); // Find user email EditText by ID
        userPassword = findViewById(R.id.passwordLogin); // Find user password EditText by ID
        btnLogin = findViewById(R.id.btnLogin); // Find login button TextView by ID
        btnSignup = findViewById(R.id.btnSignup); // Find signup button TextView by ID

        // Set click listener for login button
        btnLogin.setOnClickListener(new View.OnClickListener() {
            // Handle button click-> Get email and password from EditText then Check if email is empty
            @Override
            public void onClick(View v) {
                email = userEmail.getText().toString().trim();
                passsword = userPassword.getText().toString().trim();
                if (TextUtils.isEmpty(email)) {
                    // Set error message
                    userEmail.setError("Please Enter our Email");
                    // Request focus on email EditText
                    userEmail.requestFocus();
                    return;
                }
                // Check if password is empty, Set error message and Request focus on password EditText
                if (TextUtils.isEmpty(passsword)) {
                    userPassword.setError("Please Enter our Email");
                    userPassword.requestFocus();
                    return;
                }
                // Call SignIn method
                SignIn();
            }
        });

        // Set click listener for signup button
        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Create intent for SignUpActivity and then Start SignUpActivity
                Intent intent = new Intent(SignInActivity.this, SignUpAcrivity.class);
                startActivity(intent);
            }
        });


        //Original code to set window insets listener for edge-to-edge display
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }




    // Method to sign in user with email and password
    //Handle success and failure
    private void SignIn() {
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email.trim(), passsword)
                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        //passing the same name to Sign in
                        String username = FirebaseAuth.getInstance().getCurrentUser().getDisplayName(); // Get user's display name
                        String userEmail = FirebaseAuth.getInstance().getCurrentUser().getEmail(); // Get user's email
                        Intent intent = new Intent(SignInActivity.this, MainActivity.class); // Create intent for MainActivity
                        //passing name As user Name
                        intent.putExtra("UserName", username);
                        startActivity(intent);
                        finish();

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Check if exception is FirebaseApiNotAvailableException
                        if (e instanceof FirebaseApiNotAvailableException) {
                            Toast.makeText(SignInActivity.this, "User dosent Exist", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(SignInActivity.this, "SignInActivity Failed", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }

}