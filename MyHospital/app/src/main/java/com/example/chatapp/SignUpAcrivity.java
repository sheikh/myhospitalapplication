package com.example.chatapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Objects;


public class SignUpAcrivity extends AppCompatActivity {
    EditText userEmail, userPassword, userName, userLastname, userAddress; // Declare EditTexts for user information
    TextView btnSignup; // Declare TextView for signup button
    String txtEmail, txtPassword, txtName, txtLastname, txtuserAddress, txtDepartment, txtRole, txtRoomNumber; // Declare strings for user information
    Spinner userSpinnerRole, userSpinnerDepartment, userSpinnerRoomNumber; // Declare Spinners for role, department, and room number
    DatabaseReference databaseReference; // Declare DatabaseReference for Firebase


    // Suppress lint warning
    // Define Sign Up Activity class extending AppCompatActivity
    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_signup);

        // Get reference to Firebase database
        databaseReference = FirebaseDatabase.getInstance().getReference("MyHospital/Users");


        userEmail = findViewById(R.id.emailSignup); // Find user email EditText by ID
        userPassword = findViewById(R.id.passwordSignup); // Find user password EditText by ID
        userName = findViewById(R.id.nameSignup); // Find user name EditText by ID
        userLastname = findViewById(R.id.lastnameSignup); // Find user last name EditText by ID
        userAddress = findViewById(R.id.fullAddress); // Find user address EditText by ID
        userSpinnerRoomNumber = findViewById(R.id.roomNummber); // Find room number Spinner by ID
        userSpinnerDepartment = findViewById(R.id.spinnerDepartment); // Find department Spinner by ID
        userSpinnerRole = findViewById(R.id.spinnerRole); // Find role Spinner by ID
        btnSignup = findViewById(R.id.sbtnSignup); // Find signup button TextView by ID

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> updateAdapterDepartment = ArrayAdapter.createFromResource(this,
                R.array.department_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        updateAdapterDepartment.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        userSpinnerDepartment.setAdapter(updateAdapterDepartment);


        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> updateAdapterRole = ArrayAdapter.createFromResource(this,
                R.array.Role_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        updateAdapterRole.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        userSpinnerRole.setAdapter(updateAdapterRole);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> updateAdapterRoomNumber = ArrayAdapter.createFromResource(this,
                R.array.Room_numbers_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        updateAdapterRoomNumber.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        userSpinnerRoomNumber.setAdapter(updateAdapterRoomNumber);

        // Set click listener for signup button and Handle button click
        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtEmail = userEmail.getText().toString().toLowerCase().trim(); // Get email from EditText
                txtPassword = userPassword.getText().toString().toLowerCase().trim(); // Get password from EditText
                txtName = userName.getText().toString().toLowerCase().trim(); // Get name from EditText
                txtLastname = userLastname.getText().toString().toLowerCase().trim(); // Get last name from EditText
                txtuserAddress = userAddress.getText().toString().toLowerCase().trim(); // Get address from EditText
                txtRoomNumber = userSpinnerRoomNumber.getSelectedItem().toString().toLowerCase().trim(); // Get selected room number
                txtDepartment = userSpinnerDepartment.getSelectedItem().toString().trim(); // Get selected department
                txtRole = userSpinnerRole.getSelectedItem().toString().toLowerCase().trim(); // Get selected role



                if (TextUtils.isEmpty(txtEmail) || TextUtils.isEmpty(txtPassword) || TextUtils.isEmpty(txtLastname) || TextUtils.isEmpty(txtuserAddress)) { // Check if any required field is empty
                    userEmail.setError("Enter All Values"); // Set error message
                    userEmail.requestFocus(); // Request focus on email EditText
                } else if (txtPassword.length() < 6) { // Check if password is too short
                    Toast.makeText(SignUpAcrivity.this, "Password too short", Toast.LENGTH_SHORT).show(); // Show toast message
                } else {
                    signUp(txtEmail, txtPassword); // Call signUp method
                }
            }

        });


        //Original code to set window insets listener for edge-to-edge display
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }




    // Method to sign up user
    private void signUp(String email, String password) {
        // Initialize FirebaseAuth instance
        FirebaseAuth auth = FirebaseAuth.getInstance();

        // Create user with email and password then Handle success then Handle failure
        auth.createUserWithEmailAndPassword(txtEmail.trim(), txtPassword)
                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        // Create user profile change request and Set display name
                        UserProfileChangeRequest userProfileChangeRequest = new UserProfileChangeRequest.Builder()
                                .setDisplayName(SignUpAcrivity.this.txtName).build();

                        // Get current Firebase user
                        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                        // Check if Firebase user is not null
                        if (firebaseUser != null) {
                            // Update user profile
                            firebaseUser.updateProfile(userProfileChangeRequest);

                            // Create user model,Get reference to Firebase database and save User´s data(information) to DataBase,
                            UserModel userModel = new UserModel(FirebaseAuth.getInstance().getUid(), SignUpAcrivity.this.txtName, txtEmail, txtLastname, txtuserAddress, txtDepartment, txtRole, txtRoomNumber);
                            DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("MyHospital/Users");
                            databaseReference.child(Objects.requireNonNull(FirebaseAuth.getInstance().getUid())).setValue(userModel);

                            // Create intent for starting  MainActivity
                            Intent intent = new Intent(SignUpAcrivity.this, MainActivity.class);
                            //passing name As user Name
                            intent.putExtra("UserName", SignUpAcrivity.this.txtName);


                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(SignUpAcrivity.this, "Failed to get Firebase user!", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(SignUpAcrivity.this, "Sign Up Failed: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }



}