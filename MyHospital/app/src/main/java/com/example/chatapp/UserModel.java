package com.example.chatapp;

// Define UserModel class in Main Activity to provides a structured way to store and manage Users data.
// This makes it easy to pass the Users Data objects around the application,
// such as saving them to a database or displaying them in a UI.
public class UserModel {
    String userID, userName, userEmail, userLastname, userFullAddress, userDepartment, userRole, userRoomNumber;
    private long timestamp;
    private String Fullname, DateFrom, Time, Department, ShiftSelection, Details, Colleg,SectionSelect;
    private String messageId;
    private String senderID;
    private String message;
    private String senderRoom, recieverRoom;
    private String ShiftsID,ShangeRequist;



    // Default constructor required for calls to DataSnapshot
    public UserModel() {
    }

    public UserModel(String userID, String userName, String userEmail, String userLastname, String userFullAddress, String userRole, String userDepartment, String roomNumber) {
        this.userID = userID;
        this.userName = userName;
        this.userEmail = userEmail;
        this.userLastname = userLastname;
        this.userFullAddress = userFullAddress;
        this.userDepartment = userDepartment;
        this.userRole = userRole;
        this.userRoomNumber = roomNumber;
        this.timestamp = System.currentTimeMillis();
    }


    public String getShangeRequist() {
        return ShangeRequist;
    }

    public void setShangeRequist(String shangeRequist) {
        ShangeRequist = shangeRequist;
    }

    public String getSenderID() {
        return senderID;
    }

    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    // Getters and Setters for all fields
    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserLastname() {
        return userLastname;
    }

    public void setUserLastname(String userLastname) {
        this.userLastname = userLastname;
    }

    public String getUserFullAddress() {
        return userFullAddress;
    }

    public void setUserFullAddress(String userFullAddress) {
        this.userFullAddress = userFullAddress;
    }

    public String getUserDepartment() {
        return userDepartment;
    }

    public void setUserDepartment(String userDepartment) {
        this.userDepartment = userDepartment;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getUserRoomNumber() {
        return userRoomNumber;
    }

    public void setUserRoomNumber(String userRoomNumber) {
        this.userRoomNumber = userRoomNumber;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getFullname() {
        return getUserName()+" "+getUserLastname();
    }

    public void setFullname(String fullname) {
        Fullname = fullname;
    }

    public String getDateFrom() {
        return DateFrom;
    }

    public void setDateFrom(String dateFrom) {
        DateFrom = dateFrom;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getDepartment() {
        return Department;
    }

    public void setDepartment(String department) {
        Department = department;
    }

    public String getShiftSelection() {
        return ShiftSelection;
    }

    public void setShiftSelection(String shiftSelection) {
        ShiftSelection = shiftSelection;
    }

    public String getDetails() {
        return Details;
    }

    public void setDetails(String details) {
        Details = details;
    }

    public String getColleg() {
        return Colleg;
    }

    public void setColleg(String colleg) {
        Colleg = colleg;
    }


    public String getSenderRoom() {
        return senderRoom;
    }

    public void setSenderRoom(String senderRoom) {
        this.senderRoom = senderRoom;
    }

    public String getRecieverRoom() {
        return recieverRoom;
    }

    public void setRecieverRoom(String recieverRoom) {
        this.recieverRoom = recieverRoom;
    }

    public String getShiftsID() {
        return ShiftsID;
    }

    public void setShiftsID(String shiftsID) {
        ShiftsID = shiftsID;
    }

    public String getSectionSelect() {
        return SectionSelect;
    }

    public void setSectionSelect(String sectionSelect) {
        SectionSelect = sectionSelect;
    }
}
