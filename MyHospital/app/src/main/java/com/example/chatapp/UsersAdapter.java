package com.example.chatapp;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.icu.util.Calendar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

// Define UsersAdapter class extending RecyclerView.Adapter
public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.MyViewHolder> {
    private Context context; // Declare context
    private List<UserModel> userModelList; // Declare list for user models
    private List<UserModel> userModelListFull; // Declare list for full user models
    private FirebaseAuth auth; // Declare FirebaseAuth
    private FirebaseDatabase firebaseDatabase; // Declare FirebaseDatabase
    private DatabaseReference databaseReference; // Declare DatabaseReference
    private String loggedInUserEmail; // Declare string for logged in user's email

    // Constructor for UsersAdapter
    public UsersAdapter(Context context) {
        this.context = context; // Initialize context
        this.userModelList = new ArrayList<>(); // Initialize user model list
        this.userModelListFull = new ArrayList<>(userModelList); // Initialize full user model list
        this.auth = FirebaseAuth.getInstance(); // Initialize FirebaseAuth
        this.firebaseDatabase = FirebaseDatabase.getInstance(); // Initialize FirebaseDatabase
        this.databaseReference = firebaseDatabase.getReference("MyHospital/Users"); // Initialize DatabaseReference
        this.loggedInUserEmail = getCurrentUserEmail(); // Initialize logged in user's email
    }

    public void add(UserModel userModel) { // Method to add a user model
        userModelList.add(userModel); // Add user model to list
        userModelListFull.add(userModel); // Add user model to full list
        notifyDataSetChanged(); // Notify adapter of data change
    }

    @SuppressLint("NotifyDataSetChanged") // Suppress lint warning
    public void clear() { // Method to clear user models
        userModelList.clear(); // Clear user model list
        userModelListFull.clear(); // Clear full user model list
        notifyDataSetChanged(); // Notify adapter of data change
    }


    // Create view holder, Inflate user row layout and Return view holder
    @NonNull
    @Override
    public UsersAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        @SuppressLint("RestrictedApi")
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_row, parent, false);// Inflate user row layout and Return view holder
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UsersAdapter.MyViewHolder holder, int position) {
        UserModel userModel = userModelList.get(position); // Get user model at position
        holder.name.setText(userModel.getUserName().toLowerCase().trim()); // Set user name
        holder.lastname.setText(userModel.getUserLastname().toLowerCase().trim()); // Set user last name
        holder.department.setText(userModel.getUserDepartment().toLowerCase().trim()); // Set user department
        holder.role.setText(userModel.getUserRole().toLowerCase().trim()); // Set user role
        holder.roomNumber.setText(userModel.getUserRoomNumber().toLowerCase().trim()); // Set user room number
        holder.email.setText(userModel.getUserEmail().toLowerCase().trim()); // Set user email

        // Method to get the current user's email to Set buttons Visibility
        if (auth.getCurrentUser() != null && "loureen@gmail.com".equals(auth.getCurrentUser().getEmail())) {
            // Show add shift and deleteSift button if current user is shift lieder
            holder.btnAddSchift.setVisibility(View.VISIBLE);
            holder.btnDeleteUser.setVisibility(View.VISIBLE);
        } else {
            // hide add shift and delete shift  button if current user is not SHift Lieder
            holder.btnAddSchift.setVisibility(View.GONE);
            holder.btnDeleteUser.setVisibility(View.GONE);
        }
        // Set item click listener
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Create intent for ChatActivity to start the Chat Activity and passing the UserID and UserName
                Intent intent = new Intent(context, ChatActivity.class);
                intent.putExtra("userID", userModel.getUserID());
                intent.putExtra("userName", userModel.getUserName());
                context.startActivity(intent);
            }
        });

        // Set delete button click listener if in holder delete button is clicked
        holder.itemView.findViewById(R.id.btnDeleteUser).setOnClickListener(new View.OnClickListener() {
            // if Clicked then show first deletion confirmation
            @Override
            public void onClick(View v) {
                showDeleteConfirmationDialog(userModel, holder);
            }
        });

        // Set add shift button click listener
        holder.btnAddSchift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // if Clicked then Create a new DialogPlus dialog then Set content holder to popup_add_shift layout
                //and Set dialog to be expanded with a height of 2800
                final DialogPlus dialogPlus = DialogPlus.newDialog(holder.name.getContext())
                        .setContentHolder(new ViewHolder(R.layout.popup_add_schift))
                        .setExpanded(true, 2800)
                        .create();

                // Check if current user is Shift Leader
                if (checkIfAdmin()) {
                    // Generate a new ShiftsID and then show dialog
                    String shiftsID = databaseReference.child("Shifts").push().getKey();
                    dialogPlus.show();

                    // Get dialog view
                    View view = dialogPlus.getHolderView();

                    TextView Fullname = view.findViewById(R.id.username); // Find username TextView by ID
                    TextView ShiftsID = view.findViewById(R.id.ShiftsID); // Find ShiftsID TextView by ID
                    TextView UserID = view.findViewById(R.id.userID); // Find userID TextView by ID
                    TextView time = view.findViewById(R.id.myTime); // Find time TextView by ID
                    TextView dateFrom = view.findViewById(R.id.DateFrom); // Find dateFrom TextView by ID
                    EditText shiftDetails = view.findViewById(R.id.pShiftDetails); // Find shiftDetails EditText by ID
                    EditText colleagues = view.findViewById(R.id.pColleagues); // Find colleagues EditText by ID

                    Spinner departmentSpinner = view.findViewById(R.id.pDepartment); // Find department Spinner by ID
                    ArrayAdapter<CharSequence> adapterDepartment = ArrayAdapter.createFromResource(holder.name.getContext(),
                            R.array.department_array, android.R.layout.simple_spinner_item); // Create adapter for department spinner
                    adapterDepartment.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // Set dropdown view resource
                    departmentSpinner.setAdapter(adapterDepartment); // Set adapter for department spinner

                    Spinner ShiftSpinner = view.findViewById(R.id.pShiftSelection); // Find shift selection Spinner by ID
                    ArrayAdapter<CharSequence> adapterShift = ArrayAdapter.createFromResource(holder.name.getContext(),
                            R.array.shift_Time_array, android.R.layout.simple_spinner_item); // Create adapter for shift spinner
                    adapterShift.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // Set dropdown view resource
                    ShiftSpinner.setAdapter(adapterShift); // Set adapter for shift spinner

                    Spinner SectionSpinner = view.findViewById(R.id.pSection); // Find section Spinner by ID
                    ArrayAdapter<CharSequence> adapterSection = ArrayAdapter.createFromResource(holder.name.getContext(),
                            R.array.Section_Array, android.R.layout.simple_spinner_item); // Create adapter for section spinner
                    adapterSection.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // Set dropdown view resource
                    SectionSpinner.setAdapter(adapterSection); // Set adapter for section spinner

                    Button btnAddSchiftDialog = view.findViewById(R.id.btnAddSchiftDialog); // Find add shift dialog button by ID
                    Fullname.setText(userModel.getFullname()); // Set fullname TextView with user model fullname
                    UserID.setText(userModel.getUserID()); // Set userID TextView with user model ID
                    ShiftsID.setText(shiftsID); // Set ShiftsID TextView with generated ShiftsID

                    shiftDetails.setText(userModel.getDetails()); // Set shift details EditText with user model details
                    colleagues.setText(userModel.getColleg()); // Set colleagues EditText with user model colleagues

                    // Check if user model time is not null then Set time TextView with user model time
                    if (userModel.getTime() != null) {
                        time.setText(userModel.getTime());
                    } else {
                        String currentTime = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.getDefault()).format(new Date());
                        time.setText(currentTime);
                    }

                    // Set date from TextView with "Select Date " text
                    dateFrom.setText("Select Date");
                    // Set date from click listener
                    dateFrom.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Calendar mCurrentDate = Calendar.getInstance(); // Get current date
                            int year = mCurrentDate.get(Calendar.YEAR); // Get current year
                            int month = mCurrentDate.get(Calendar.MONTH); // Get current month
                            int day = mCurrentDate.get(Calendar.DAY_OF_MONTH); // Get current day

                            DatePickerDialog datePickerDialog = new DatePickerDialog(holder.name.getContext(), new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker datePicker, int selectedYear, int selectedMonth, int selectedDay) {
                                    // Increment month by 1
                                    selectedMonth = selectedMonth + 1;
                                    // Set date format
                                    dateFrom.setText(String.format(Locale.getDefault(), "%02d.%02d.%d", selectedDay, selectedMonth, selectedYear));
                                }
                            }, year, month, day); // Initialize date picker dialog with current date

                            datePickerDialog.setTitle("Select Date"); // Set date picker dialog title
                            datePickerDialog.show(); // Show date picker dialog
                        }
                    });

                    btnAddSchiftDialog.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            btnAddSchiftDialog.setEnabled(false); // Disable add shift dialog button
                            String selectedDate = dateFrom.getText().toString();
                            String currentTime = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.getDefault()).format(new Date()); // Get current time
                            String department = departmentSpinner.getSelectedItem().toString(); // Get selected department
                            String shift = ShiftSpinner.getSelectedItem().toString(); // Get selected shift
                            String sectionSelect = SectionSpinner.getSelectedItem().toString(); // Get selected section

                            // Create map for shift details
                            Map<String, Object> shiftData = new HashMap<>();
                            shiftData.put("DateFrom", selectedDate.toLowerCase());
                            shiftData.put("UserName", userModel.getFullname().toLowerCase());
                            shiftData.put("ShiftsID", ShiftsID.getText().toString());
                            shiftData.put("UserID", UserID.getText().toString());
                            shiftData.put("Department", department.toLowerCase());
                            shiftData.put("SectionSelect", sectionSelect.toLowerCase());
                            shiftData.put("ShiftSelection", shift.toLowerCase());
                            shiftData.put("Details", shiftDetails.getText().toString().toLowerCase());
                            shiftData.put("Colleg", colleagues.getText().toString().toLowerCase());
                            shiftData.put("Time", currentTime);

                            // Use a Firebase transaction to check and add shift
                            databaseReference.child("Shifts").child(userModel.getUserID()).runTransaction(new Transaction.Handler() {
                                @NonNull
                                @Override
                                public Transaction.Result doTransaction(@NonNull MutableData mutableData) {
                                    for (MutableData child : mutableData.getChildren()) {
                                        String existingDate = child.child("DateFrom").getValue(String.class);
                                        if (existingDate != null && existingDate.equalsIgnoreCase(selectedDate)) {
                                            return Transaction.abort(); // Abort if shift already exists for the selected date
                                        }
                                    }
                                    mutableData.child(shiftsID).setValue(shiftData);
                                    return Transaction.success(mutableData);
                                }

                                @Override
                                public void onComplete(DatabaseError databaseError, boolean committed, DataSnapshot dataSnapshot) {
                                    if (committed) {
                                        // Show success message and dismiss dialog
                                        Toast.makeText(holder.name.getContext(), userModel.getFullname().toUpperCase() + " Shift is Added Successfully", Toast.LENGTH_SHORT).show();
                                        dialogPlus.dismiss();
                                    } else {
                                        // Show failure message and re-enable button
                                        Toast.makeText(holder.name.getContext(), "Shift already exists on this date or Error while Adding Shift", Toast.LENGTH_SHORT).show();
                                        btnAddSchiftDialog.setEnabled(true);
                                    }
                                }
                            });
                        }
                    });
                } else {
                    // Show error message
                    Toast.makeText(holder.name.getContext(), "You do not have permission to Add New Shift", Toast.LENGTH_LONG).show();
                }
            }
        });


    }


    // Method to show delete confirmation dialog
    private void showDeleteConfirmationDialog(UserModel userModel, MyViewHolder holder) {
        if (checkIfAdmin()) {
            // Create an AlertDialog builder and Set dialog title then Set dialog message
            AlertDialog.Builder builder = new AlertDialog.Builder(holder.name.getContext());
            builder.setTitle("Are you sure to DELETE " + userModel.getUserName() + " " + userModel.getUserLastname());
            builder.setMessage("Deleted data can't be undone");
            // Set positive button and click listener
            builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // Call method to delete user from Firebase
                    deleteUserFromFirebase(userModel);
                }
            });
            // Set negative button and click listener
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(holder.name.getContext(), "Cancelled", Toast.LENGTH_SHORT).show();
                }
            });
            // Show the dialog
            builder.show();
        } else {
            Toast.makeText(holder.name.getContext(), "You do not have permission to delete users.", Toast.LENGTH_LONG).show();
        }
    }

    // Method to delete user from Firebase by getting user ID
    private void deleteUserFromFirebase(UserModel userModel) {
        String userId = userModel.getUserID();
        // Fetch sign-in methods for user's email
        auth.fetchSignInMethodsForEmail(userModel.getUserEmail())
                .addOnCompleteListener(task -> {
                    // Check if sign-in methods exist then Get current Firebase user
                    if (task.isSuccessful() && task.getResult() != null && !task.getResult().getSignInMethods().isEmpty()) {
                        FirebaseUser user = auth.getCurrentUser();
                        if (user != null) {
                            // Delete user from authentication and Remove user from database
                            user.delete().addOnCompleteListener(deleteTask -> {
                                if (deleteTask.isSuccessful()) {
                                    databaseReference.child(userId).removeValue().addOnCompleteListener(dbTask -> {
                                        if (dbTask.isSuccessful()) {
                                            // Remove user from adapter
                                            removeUser(userModel);
                                            Toast.makeText(context, userModel.getUserName() + " " + userModel.getUserLastname() + "User deleted successfully", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(context, "Failed to delete" + userModel.getUserName() + " from database", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                } else {
                                    Toast.makeText(context, "Failed to delete user from authentication", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    } else {
                        // Remove user from database if no sign-in methods exist
                        databaseReference.child(userId).removeValue().addOnCompleteListener(dbTask -> {
                            if (dbTask.isSuccessful()) {
                                // Remove user from adapter
                                removeUser(userModel);
                                Toast.makeText(context, "User deleted successfully", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "Failed to delete user from database", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
    }

    // Method to remove a user model from list and from full list and then Notify adapter of data change
    @SuppressLint("NotifyDataSetChanged")
    private void removeUser(UserModel userModel) {
        userModelList.remove(userModel);
        userModelListFull.remove(userModel);
        notifyDataSetChanged();
    }

    // Method to get item count by Returning size of user model list
    @Override
    public int getItemCount() {
        return userModelList.size();
    }

    // Method to get user model list by Returning user model list
    public List<UserModel> getUserModelList() {
        return userModelList;
    }

    // Method to filter user models first must Clear user model list
    //then Check if search text is empty then Add all user models to list
    @SuppressLint("NotifyDataSetChanged")
    public void filter(String text) {
        userModelList.clear();
        if (text.trim().isEmpty()) {
            userModelList.addAll(userModelListFull);
        } else {
            // Split search text into terms
            String[] searchTerms = text.toLowerCase().trim().split("\\s+");
            // Iterate over full user model list
            for (UserModel item : userModelListFull) {
                // Iterate over search terms
                for (String term : searchTerms) {
                    if (item.getUserName().toLowerCase().contains(term) || // Check if user name contains term
                            item.getUserLastname().toLowerCase().contains(term) || // Check if user last name contains term
                            item.getUserEmail().toLowerCase().contains(term) || // Check if user email contains term
                            item.getUserDepartment().toLowerCase().contains(term) || // Check if user department contains term
                            item.getUserRole().toLowerCase().contains(term) || // Check if user role contains term
                            item.getUserRoomNumber().toLowerCase().contains(term)) { // Check if user room number contains term
                        userModelList.add(item); // Add user model to list
                        break; // Break inner loop
                    }
                }
            }
        }
        // At the End notify adapter of data change
        notifyDataSetChanged();
    }

    // Method to get the current user's email from Firebase and chack if it is not null
    // then check if the email equals to Shifts Lieder  email
    private String getCurrentUserEmail() {
        FirebaseUser user = auth.getCurrentUser();
        if (user != null) {
            return user.getEmail();
        }
        return null;
    }

    private boolean checkIfAdmin() {
        String currentUserEmail = getCurrentUserEmail();
        return currentUserEmail != null && currentUserEmail.equalsIgnoreCase("loureen@gmail.com");
    }

    // Define MyViewHolder class extending RecyclerView.ViewHolder
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView name, lastname, department, role, email, roomNumber; // Declare TextViews for user details
        ImageButton btnDeleteUser, btnSendRequist, btnAddSchift; // Declare ImageButtons for actions
        // Constructor for MyViewHolder
        public MyViewHolder(@NonNull android.view.View itemView) {
            // Call superclass constructor
            super(itemView);
            name = itemView.findViewById(R.id.username); // Find username TextView by ID
            lastname = itemView.findViewById(R.id.userLastname); // Find lastname TextView by ID
            department = itemView.findViewById(R.id.userDepartment); // Find department TextView by ID
            role = itemView.findViewById(R.id.userRolee); // Find role TextView by ID
            roomNumber = itemView.findViewById(R.id.userRoomNumnber); // Find room number TextView by ID
            email = itemView.findViewById(R.id.userEmail); // Find email TextView by ID
            btnDeleteUser = itemView.findViewById(R.id.btnDeleteUser); // Find delete user button by ID
            btnAddSchift = itemView.findViewById(R.id.btnAddSchift); // Find add shift button by ID
            btnSendRequist = itemView.findViewById(R.id.btnSendRequist); // Find send request button by ID
        }
    }
}
