package com.example.chatapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class MainActivity extends AppCompatActivity {


    RecyclerView recyclerView; // Declare RecyclerView for displaying users
    DatabaseReference databaseReference; // Declare DatabaseReference for Firebase
    UsersAdapter usersAdapter; // Declare UsersAdapter for RecyclerView


    @SuppressLint({"MissingInflatedId", "WrongViewCast"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        FirebaseApp.initializeApp(this);
        setContentView(R.layout.activity_main);


        // Find the toolbar by ID, Set the toolbar as the app bar and Set the toolbar title to empty
        Toolbar toolbar= findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Set the toolbar title to empty
        getSupportActionBar().setTitle("");
        // Fetch user's name and display welcome message
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        // Get the current user then Check if user is not null then Get user ID
        if (currentUser != null) {
            String uid = currentUser.getUid();
            // Get reference to user's data
            DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("MyHospital/Users").child(uid);

            // Get reference to user's data
            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                // Handle data change and  Get user model from snapshot
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    UserModel userModel = dataSnapshot.getValue(UserModel.class);
                    // Check if user model is not null
                    if (userModel != null) {
                        String welcomeText = userModel.getUserName().toUpperCase();  // Get user's name and convert to uppercase
                        TextView tvWelcome = findViewById(R.id.tvWelcome);// Find the welcome TextView by ID
                        tvWelcome.setText(welcomeText);// Set welcome text in TextView
                    }
                }

                // Handle database error and // Log warning message
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.w("Firebase", "Failed to read user");
                }
            });
        }



        // Initialize RecyclerView and Adapter
        usersAdapter = new UsersAdapter(this); // Initialize UsersAdapter
        recyclerView = findViewById(R.id.recycler); // Find the RecyclerView by ID
        recyclerView.setAdapter(usersAdapter); // Set adapter for RecyclerView
        recyclerView.setLayoutManager(new LinearLayoutManager(this)); // Set layout manager for RecyclerView



        // Setup SearchView by finding the id and  Set query text listener for SearchView
        SearchView searchView = findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            // Handle query text submit and Filter users based on query
            @Override
            public boolean onQueryTextSubmit(String query) {
                usersAdapter.filter(query);
                return false;
            }

            // Handle query text change and Filter users based on new text
            @Override
            public boolean onQueryTextChange(String newText) {
                usersAdapter.filter(newText);
                return false;
            }
        });



        // Initialize database reference for users and Get reference to users data
        databaseReference= FirebaseDatabase.getInstance().getReference("MyHospital/Users");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                // Handle data change and Clear adapter
                usersAdapter.clear();
                // Iterate over user snapshots
                for(DataSnapshot dataSnapshot:snapshot.getChildren()){
                    // Get user ID
                    String uId=dataSnapshot.getKey();
                    // Get user model from snapshot
                    UserModel userModel= dataSnapshot.getValue(UserModel.class);

                    // Check if user model is valid
                    if(userModel != null && userModel.getUserID() !=null && !userModel.getUserID().equals(FirebaseAuth.getInstance().getUid())){
                        //Update  if not matching and add user to Adapter
                        usersAdapter.add(userModel);

                    }
                }
                List<UserModel> userModelList= usersAdapter.getUserModelList();
                usersAdapter.notifyDataSetChanged();// Notify adapter of data change
            }

            // Handle database error and show Toast on error
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(MainActivity.this, "Database error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });




        //Original code to set window insets listener for edge-to-edge display
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    //Initializing Menu inflator for menu icons
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater= getMenuInflater();
        inflater.inflate(R.menu.main_menu,menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // Prepare menu options to handle the visibility
        MenuItem shiftAnfrage = menu.findItem(R.id.ShiftAnfrage);
        MenuItem onDayCalender = menu.findItem(R.id.onDayCalender);



        // Logic to set visibility
        boolean isLoggedIn = checkUserLoggedIn();
        shiftAnfrage.setVisible(isLoggedIn);
        onDayCalender.setVisible(!isLoggedIn);



        return super.onPrepareOptionsMenu(menu);
    }
    //extra main_menu icons added and handelt
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()== R.id.logout){
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(MainActivity.this,SignInActivity.class));
            finish();
            return true;
        }
        if(item.getItemId()== R.id.onDayCalender){
            FirebaseAuth.getInstance();
            startActivity(new Intent(MainActivity.this, MyCalender.class));
            finish();
            return true;
        }

        if(item.getItemId()== R.id.ShowAllShifts){
            FirebaseAuth.getInstance();
            startActivity(new Intent(MainActivity.this, AllCalender.class));
            finish();
            return true;
        }
        if(item.getItemId()== R.id.ShiftAnfrage){

            FirebaseAuth.getInstance();
            startActivity(new Intent(MainActivity.this, RequistChange.class));
            finish();
            return true;
        }
        if(item.getItemId()== R.id.listShift){

            FirebaseAuth.getInstance();
            startActivity(new Intent(MainActivity.this, TextActivity.class));
            finish();
            return true;
        }


        return false;

    }
    // Check if user is logged in as Shifts lieder email
    // And return true if email matches.
    private boolean checkUserLoggedIn() {
        String loggedInUserEmail = getLoggedInUserEmail();
        return "loureen@gmail.com".equals(loggedInUserEmail);
    }

    // Get the logged in user's email
    // Get current Firebase user and check If user is not null then return the user´s email
    // otherwise return null.
    private String getLoggedInUserEmail() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            return user.getEmail();
        } else {
            return null;
        }
    }


}