package com.example.chatapp;


import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MyViewHolder> {
    private static final String CHANNEL_ID = "chat_app_notifications"; // Define channel ID for notifications
    private Context context; // Declare context
    private List<MessageModel> MessageModelList; // Declare list for message models
    // to store the full list
    private List<MessageModel> MessageModelListFull; // Declare list for full message models
    private static final int VIEW_TYPE_SEND = 1; // Define view type for sent messages
    private static final int VIEW_TYPE_RECIVED = 2; // Define view type for received messages


    public MessageAdapter(Context context) { // Constructor for MessageAdapter
        this.context = context; // Initialize context
        this.MessageModelList = new ArrayList<>(); // Initialize message model list
        this.MessageModelListFull = new ArrayList<>(MessageModelList); // Copy the full list
        createNotificationChannel(); // Create notification channel
    }


    // Add user Model List
    @SuppressLint("NotifyDataSetChanged") // Suppress lint warning
    public void add(MessageModel MessageModel) { // Method to add a message model
        MessageModelList.add(MessageModel); // Add message to list
        MessageModelListFull.add(MessageModel); // Add message to full list
        notifyDataSetChanged(); // Notify adapter of data change
        if (!MessageModel.getSenderID().equals(FirebaseAuth.getInstance().getUid())) { // If message is not from current user
            showNotification(MessageModel); // Show notification
        }


    }

    @SuppressLint("NotifyDataSetChanged") // Suppress lint warning
    public void clear() { // Method to clear messages
        MessageModelList.clear(); // Clear message list
        MessageModelListFull.clear(); // Clear full message list
        notifyDataSetChanged(); // Notify adapter of data change
    }

    @NonNull
    @Override
    public MessageAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) { // Create view holder
        LayoutInflater inflater = LayoutInflater.from(parent.getContext()); // Get layout inflater
        if (viewType == VIEW_TYPE_SEND) { // Check if view type is for sent messages
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_row_send, parent, false); // Inflate sent message layout
            return new MyViewHolder(view); // Return view holder for sent message
        } else { // If view type is for received messages
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_row_recived, parent, false); // Inflate received message layout
            return new MyViewHolder(view); // Return view holder for received message
        }
    }

    @Override
    public int getItemViewType(int position) { // Get item view type
        if (getMessageModelList().get(position).getSenderID().equals(FirebaseAuth.getInstance().getUid())) { // Check if message is from current user
            return VIEW_TYPE_SEND; // Return view type for sent message
        } else { // If message is from other user
            return VIEW_TYPE_RECIVED; // Return view type for received message
        }
    }

    @Override
    public void onBindViewHolder(@NonNull MessageAdapter.MyViewHolder holder, int position) { // Bind view holder
        MessageModel MessageModel = MessageModelList.get(position); // Get message model for position

        if (MessageModel.getSenderID().equals(FirebaseAuth.getInstance().getUid())) { // Check if message is from current user
            holder.txtViewSendMessage.setText(MessageModel.getMessage()); // Set text for sent message
        } else { // If message is from other user
            holder.txtViewRecivedMessage.setText(MessageModel.getMessage()); // Set text for received message
        }
    }

    @Override
    public int getItemCount() { // Get item count
        return MessageModelList.size(); // Return size of message model list
    }

    public List<MessageModel> getMessageModelList() { // Get message model list
        return MessageModelList; // Return message model list
    }

    // Define MyViewHolder class and  Declare TextViews for sent and received messages
    public class MyViewHolder extends RecyclerView.ViewHolder {
        //define Card View user_row xml
        private TextView txtViewSendMessage, txtViewRecivedMessage;

        // Constructor for MyViewHolder,Call superclass constructor
        public MyViewHolder(@NonNull android.view.View itemView) {
            super(itemView);
            // Find TextView for sent message by ID
            txtViewSendMessage = itemView.findViewById(R.id.tvSendMessage);
            // Find TextView for received message by ID
            txtViewRecivedMessage = itemView.findViewById(R.id.tvRecievedMessage);

        }


    }

    // Method to create notification channel
// Method to create notification channel
    private void createNotificationChannel() {
        // Check if SDK version is Oreo or higher
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Set channel name
            CharSequence name = "Chat Notifications";
            // Set channel description
            String description = "Notifications for new chat messages";
            // Create notification channel
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Get notification manager
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            // Create notification channel
            notificationManager.createNotificationChannel(channel);
        }
    }

    // Method to show notification
    private void showNotification(MessageModel messageModel) {
        Intent intent = new Intent(context, MainActivity.class); // Open MainActivity when the notification is clicked
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); // Set intent flags

        // Create pending intent
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID) // Build notification
                .setSmallIcon(R.drawable.add_msg) // Set small icon
                .setContentTitle("New Message") // Set content title
                .setContentText(messageModel.getMessage()) // Set content text
                .setPriority(NotificationCompat.PRIORITY_DEFAULT) // Set priority
                .setContentIntent(pendingIntent) // Set the pending intent to be triggered when the notification is clicked
                .setAutoCancel(true); // Automatically remove the notification when clicked

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE); // Get notification manager

        // Use a constant notification ID to ensure only one notification is shown
        int notificationId = 1; // Use a constant ID for all notifications
        notificationManager.notify(notificationId, builder.build()); // Show notification
    }





}

