package com.example.chatapp;

// Define RequistModel class to provides a structured way to store and manage Change Request data.
// This makes it easy to pass the Requested objects around the application,
// such as saving them to a database or displaying them in a UI.
public class RequistModel {
   private String UserID,Name,DateFrom,Time,Department,ShiftSelection,Details,ChangeRequist,ShiftsID,RequistID;
    private long timestamp;

    // Default constructor required for calls to DataSnapshot
    public RequistModel() {
    }



    public RequistModel(String requistID,String userID,String shiftId, String userName, String dateFrom,  String time, String department, String shiftSelection, String details, String shangeRequist) {

        UserID= userID;
        Name = userName;
        DateFrom = dateFrom;
        Time = time;
        Department = department;
        ShiftSelection = shiftSelection;
        Details = details;
        ChangeRequist = shangeRequist;
        this.timestamp = System.currentTimeMillis();
        ShiftsID=shiftId;
        RequistID=requistID;
    }


    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDateFrom() {
        return DateFrom;
    }

    public void setDateFrom(String dateFrom) {
        DateFrom = dateFrom;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getDepartment() {
        return Department;
    }

    public void setDepartment(String department) {
        Department = department;
    }

    public String getShiftSelection() {
        return ShiftSelection;
    }

    public void setShiftSelection(String shiftSelection) {
        ShiftSelection = shiftSelection;
    }

    public String getDetails() {
        return Details;
    }

    public void setDetails(String details) {
        Details = details;
    }

    public String getChangeRequist() {
        return ChangeRequist;
    }

    public void setChangeRequist(String changeRequist) {
        ChangeRequist = changeRequist;
    }

    public String getShiftsID() {
        return ShiftsID;
    }

    public void setShiftsID(String shiftsID) {
        ShiftsID = shiftsID;
    }

    public String getRequistID() {
        return RequistID;
    }

    public void setRequistID(String requistID) {
        RequistID = requistID;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
