package com.example.chatapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AllCalender extends AppCompatActivity {
    private CalendarView calendarView; // CalendarView for displaying calendar
    private Button btnMain; // Button for main actions
    private DatabaseReference databaseReference; // Reference to Firebase database

    private TextView tvShiftDetails;  // TextView for displaying shift details


    @SuppressLint("MissingInflatedId") // Suppress lint warning for missing inflated ID
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);// Call superclass method
        FirebaseApp.initializeApp(this);// Initialize Firebase app
        setContentView(R.layout.activity_allcalender);// Set the content view to the activity layout

        Toolbar toolbar = findViewById(R.id.toolbar);// Find the toolbar by ID
        setSupportActionBar(toolbar);// Set the toolbar as the app bar for the activity
        getSupportActionBar().setTitle(""); // Set the toolbar title to empty


        initFirebase(); // Initialize Firebase components
        setupCalendar(); // Set up the calendar view
        tvShiftDetails = findViewById(R.id.tvShiftDetails);// Find the shift details text view by ID
    }

    // Method to initialize Firebase components and Getting Firebase database reference
    private void initFirebase() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() == null) {
            Toast.makeText(this, "No signed-in user found", Toast.LENGTH_LONG).show();
            // Redirect to login activity or handle user session
        } else {

            databaseReference = FirebaseDatabase.getInstance().getReference("MyHospital/Users/Shifts");
        }
    }

    // Setting Calender and correct date formate
    private void setupCalendar() {
        calendarView = findViewById(R.id.calendarView);
        calendarView.setOnDateChangeListener((view, year, month, dayOfMonth) -> {
            @SuppressLint("DefaultLocale")
            String date = String.format("%02d.%02d.%04d", dayOfMonth, month + 1, year); // Ensuring month is correctly adjusted and zero-padded
            Log.d("AllCalender", "Selected date: " + date); // Log the selected date to ensure the correct formate
            searchDatabase(date);
        });

        //Original code to set window insets listener for edge-to-edge display
        ViewCompat.setOnApplyWindowInsetsListener(calendarView, (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }


    // Method to search the database for shifts on a specific date
    private void searchDatabase(String date) {
        // Check if database reference is initialized
        if (databaseReference == null) {
            Toast.makeText(this, "Database reference is not initialized", Toast.LENGTH_SHORT).show();
            return;
        }


        // Retrieve shift details from Firebase for the selected date
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            // Handle data change
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                StringBuilder detailsBuilder = new StringBuilder();// StringBuilder for shift details
                List<TextModel> shiftsList = new ArrayList<>();// List to store shifts

                // Iterate over user snapshots
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    // Iterate over shift snapshots
                    for (DataSnapshot shiftSnapshot : userSnapshot.getChildren()) {
                        // Get shift as TextModel
                        TextModel shift = shiftSnapshot.getValue(TextModel.class);
                        // Check if shift is not null
                        if (shift != null) {
                            // Check if shift date matches the searched date
                            if (date.equals(shift.getDateFrom())) {
                                // Then add shift to the list
                                shiftsList.add(shift);
                            }
                        } else {
                            //Toast an message Shift is null for Shift ID
                            Toast.makeText(AllCalender.this, "Shift is null for Shift ID:"+shiftSnapshot.getKey(), Toast.LENGTH_SHORT).show();

                        }
                    }
                }

                // Sort shiftsList by shiftSelection
                Collections.sort(shiftsList, new Comparator<TextModel>() {
                    @Override
                    public int compare(TextModel shift1, TextModel shift2) {
                        return shift1.getShiftSelection().compareTo(shift2.getShiftSelection());
                    }
                });

                // Display the sorted shifts
                for (TextModel shift : shiftsList) {
                    detailsBuilder
                            .append("\nName: ").append(shift.getUserName())
                            .append("\nDate : ").append(shift.getDateFrom())
                            .append("\nAt: ").append(shift.getShiftSelection())
                            .append("\nDepartment: ").append(shift.getDepartment())
                            .append("\nShift Room: ").append(shift.getSectionSelect())
                            .append("\nColleague: ").append(shift.getColleg())
                            .append("\n───────────────────\n");
                }

                if (detailsBuilder.length() == 0) {
                    tvShiftDetails.setText("No shifts found for: " + date);
                } else {
                    tvShiftDetails.setText(detailsBuilder.toString());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(AllCalender.this, "Error fetching data: " + databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e("AllCalender", "Database error: " + databaseError.getMessage()); // Log the error
            }
        });
    }

    // Overriding the onCreateOptionsMenu
    // Inflate the menu options and Get menu inflater
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_calender, menu); // Inflate the Calender menu and return true to display the menu
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        //define the MenueItems and then set the visibility
        MenuItem shiftAnfrage = menu.findItem(R.id.ShiftAnfrage);
        MenuItem onDayCalender = menu.findItem(R.id.onDayCalender);


        // Logic to set visibility who can see the Items
        //if admin he is the only to see the Change Request.
        boolean isLoggedIn = checkUserLoggedIn();
        shiftAnfrage.setVisible(isLoggedIn);
        //onDayCalender only can be seen by others than the Admin
        onDayCalender.setVisible(!isLoggedIn);


        return super.onPrepareOptionsMenu(menu);
    }

    //extra main_menu icons added and handelt
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.ShiftAnfrage) {
            // only Shift Leiter can see this Activity If Shifts Request icon selected then start Request activity where the Requests can be shown
            // Finish current activity and return true as item handled
            FirebaseAuth.getInstance();
            startActivity(new Intent(AllCalender.this, RequistChange.class));
            finish();
            return true;
        }
        // If Send Message icon selected then start Main activity where the Message can be sended
        // Finish current activity and return true as item handled
        if (item.getItemId() == R.id.sendMessage) {
            FirebaseAuth.getInstance();
            startActivity(new Intent(AllCalender.this, MainActivity.class));
            finish();
            return true;
        }
        // If Shift List icon selected then start TextActivity activity where the list displayed
        // Finish current activity and return true as item handled

        if (item.getItemId() == R.id.listShift) {
            FirebaseAuth.getInstance();
            startActivity(new Intent(AllCalender.this, TextActivity.class));
            finish();
            return true;
        }
        // If on day calendar icon selected then start my calendar activity
        // Finish current activity and return true as item handled
        if (item.getItemId() == R.id.onDayCalender) {
            startActivity(new Intent(AllCalender.this, MyCalender.class));
            finish();
            return true;
        }
        // If home icon selected then Start main activity then Finish current activity
        // And return true as item handled
        if (item.getItemId() == R.id.home) {
            startActivity(new Intent(AllCalender.this, MainActivity.class));
            finish();
            return true;
        }
        // If logout icon selected then sign out from Firebase and then start sign-in activity
        if (item.getItemId() == R.id.logout) {
            FirebaseAuth.getInstance().signOut();

            startActivity(new Intent(AllCalender.this, SignInActivity.class));
            finish();
            return true;
        }
        return false;// Return false if item not handled

    }

    // Check if user is logged in as ShiftsLeiter email
    // And return true if email matches.
    private boolean checkUserLoggedIn() {
        String loggedInUserEmail = getLoggedInUserEmail();
        return "loureen@gmail.com".equals(loggedInUserEmail);
    }

    // Get the logged in user's email
    private String getLoggedInUserEmail() {
        // Get current Firebase user and check If user is not null then return the user´s email
        // otherwise return null.
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            return user.getEmail();
        } else {
            return null;
        }
    }
}
