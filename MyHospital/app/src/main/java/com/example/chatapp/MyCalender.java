package com.example.chatapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

//Same As the All Calender but hier shows only users loggenin  Shifts in a Calender view
// it allow the user to see his Shifts in form of Calender View and then if changes required he can Handle it
public class MyCalender extends AppCompatActivity {
    private CalendarView calendarView;
    private DatabaseReference databaseReference;

    // TextView for displaying shift details
    private TextView tvShiftDetails;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseApp.initializeApp(this);
        setContentView(R.layout.activity_my_calender);

        initFirebase();
        setupCalendar();
        setupButton();
        tvShiftDetails = findViewById(R.id.tvShiftDetails);
    }

    private void initFirebase() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() == null) {
            Toast.makeText(this, "No signed-in user found", Toast.LENGTH_LONG).show();
            // Redirect to login activity or handle user session
        } else {
            databaseReference = FirebaseDatabase.getInstance().getReference("MyHospital/Users/Shifts").child(auth.getCurrentUser().getUid());
        }
    }

    private void setupCalendar() {
        calendarView = findViewById(R.id.calendarView);
        calendarView.setOnDateChangeListener((view, dayOfMonth, month, year) -> {


            @SuppressLint("DefaultLocale")
            String date = String.format("%d.%02d.%02d", year, month + 1, dayOfMonth);
            System.out.println(date);// Ensuring month is correctly adjusted
            searchDatabase(date);
        });

        ViewCompat.setOnApplyWindowInsetsListener(calendarView, (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    private void setupButton() {
        Button calendarButton = findViewById(R.id.calendarButton);
        calendarButton.setOnClickListener(v -> {
            Intent intent = new Intent(MyCalender.this, MainActivity.class);

            startActivity(intent);

        });
    }

    private void searchDatabase(String date) {
        if (databaseReference == null) {
            Toast.makeText(this, "Database reference is not initialized", Toast.LENGTH_SHORT).show();
            return;
        }

        Query query = databaseReference.orderByChild("DateFrom").equalTo(date);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    StringBuilder detailsBuilder = new StringBuilder();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        TextModel shift = snapshot.getValue(TextModel.class);
                        if (shift != null) {
                            detailsBuilder

                                    .append("Shift ID: ").append(shift.getShiftsID())
                                    .append("\nName: ").append(shift.getUserName())
                                    .append("\nShift At: ").append(shift.getShiftSelection())
                                    .append("\nDate : ").append(shift.getDateFrom())

                                    .append("\nDepartment: ").append(shift.getDepartment())
                                    .append("\nDetails: ").append(shift.getDetails())
                                    .append("\nColleague: ").append(shift.getColleg())
                                    .append("\nCreated On: ").append(shift.getTime())
                                    .append("\n───────────────────\n");

                        }
                    }
                    tvShiftDetails.setText(detailsBuilder.toString());
                } else {
                    tvShiftDetails.setText("No shifts found for: " + date);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(MyCalender.this, "Error fetching data: " + databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void displayShiftDetails(TextModel shift) {
        String details = "Shift ID: " + shift.getShiftsID() +
                "\nShift Date " + shift.getDateFrom() +
                "\nTime: " + shift.getTime() +
                "\nDepartment: " + shift.getDepartment() +
                "\nShift Selection: " + shift.getShiftSelection() +
                "\nDetails: " + shift.getDetails() +
                "\nColleague: " + shift.getColleg() +
                "\nScheduled By: " + shift.getUserName();
        Toast.makeText(this, details, Toast.LENGTH_LONG).show();
    }
}
